Simple duplicity backend for git repos.

Duplicity backend file:
  src/gitbackend.py

Remote repository must exists. The master should be empty or very small (i.e. some readme file, etc).
Each file is stored in dedicated branch. This allows to shallow clone single branch and download only single file
as well as listing remote files (branches) or removing file (branch) in order to data retention.

Restrictions:
1. Currently files should not have any space or special characters in name
2. no directories
3. no passing user/password, so currently only git via ssh transport with key setup

Those restrictions should be acceptable for duplicity backend.

Instalation:
1. copy gitbackend.py to duplicity backends directory,
    i.e. sudo cp gitbackend.py /usr/lib/python2.7/dist-packages/duplicity/
2. ensure proper ownership and perms
   sudo chown root:root /usr/lib/python2.7/dist-packages/duplicity/backends/gitbackend.py 

Test:
1. create bitbucket repo, setup ssh key, ensure you can clone master without password,
2. make backup to some testrepo at bitbucket:
   duplicity full /tmp/some/existing/dir/to/backup git+ssh://git@bitbucket.org/username/testrepo.git
3. restore backup:
   duplicity restore git+ssh://git@bitbucket.org/username/testrepo.git /tmp/restored
4. verify /tmp/some/existing/dir/to/backup/ and /tmp/restored/ are identical
