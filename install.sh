#!/bin/sh

DUPDIR=$(python -c 'import duplicity.backends, os; print os.path.dirname(duplicity.backends.__file__)')

if ! test -d "$DUPDIR"
then
    echo "Cannot find duplicity backends. Is dplicity instaled?"
    exit 1
fi

if ! test -w "$DUPDIR"
then
    echo "Do not have permission to write to $DUPDIR. How about sudo?"
    exit 2
fi

GITBACK=$(dirname $0)/src/gitbackend.py
if ! test -r "$GITBACK"
then
    echo "Cannot read $GITBACK file."
    exit 3
fi

cp $GITBACK $DUPDIR/
chown --reference "$DUPDIR/__init__.py" $DUPDIR/gitbackend.py
chgrp --reference "$DUPDIR/__init__.py" $DUPDIR/gitbackend.py
chmod --reference "$DUPDIR/__init__.py" $DUPDIR/gitbackend.py
