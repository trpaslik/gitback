# -*- Mode:Python; indent-tabs-mode:nil; tab-width:4 -*-
#
# Copyright 2015 Michal Srajer <michal@post.pl>
#
# This file is part of duplicity.
#
# Duplicity is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# Duplicity is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with duplicity; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

import os
import shutil
import tempfile
import duplicity.backend
from duplicity import log

class GitBackend(duplicity.backend.Backend):
    """Connect to remote git repository via helper scripts"""
    def __init__(self, parsed_url):
        duplicity.backend.Backend.__init__(self, parsed_url)
        # branchname prefix for file-as-a-branch
        self.file_prefix = 'file_'
        # name of single content file in file-branch
        self.inbranch_filename = 'file'
        # dirname for repo in temp_dir
        self.repo_dir_name = 'repo'
        self.url_string = parsed_url.url_string

    def __encode_name(self, file_path):
        return (self.file_prefix + file_path).encode('hex')

    def __decode_name(self, branchname):
        try:
            name = branchname.decode('hex')
        except:
            raise ValueError('wrong branch name')
        if name.startswith(self.file_prefix):
            return name[len(self.file_prefix):]
        else:
            raise ValueError('wrong branch name')

    def _put(self, source_path, remote_filename):
        temp_dir = tempfile.mkdtemp(prefix='gitback-')
        repo_dir = os.path.join(temp_dir, self.repo_dir_name)
        commandline = "git clone --single-branch --branch master --depth 1 {0} {1}".format(self.url_string, repo_dir)
        self.subprocess_popen(commandline)
        prev_dir = os.getcwd()
        os.chdir(repo_dir)
        branchname = self.__encode_name(remote_filename)
        commandline = "git checkout -b {0}".format(branchname)
        self.subprocess_popen(commandline)
        shutil.copyfile(source_path.name, self.inbranch_filename)
        commandline = "git add {0}".format(self.inbranch_filename)
        self.subprocess_popen(commandline)
        commandline = "git commit -m 'created {0}'".format(remote_filename)
        self.subprocess_popen(commandline)
        commandline = "git push origin {0}".format(branchname)
        self.subprocess_popen(commandline)
        os.chdir(prev_dir)
        shutil.rmtree(temp_dir, ignore_errors=True)

    def _get(self, remote_filename, local_path):
        temp_dir = tempfile.mkdtemp(prefix='gitback-')
        repo_dir = os.path.join(temp_dir, self.repo_dir_name)
        commandline = "git clone --single-branch --branch {0} --depth 1 {1} {2}".format(
                           self.__encode_name(remote_filename), self.url_string, repo_dir)
        self.subprocess_popen(commandline)
        shutil.copyfile(os.path.join(repo_dir, self.inbranch_filename), local_path.name)
        shutil.rmtree(temp_dir, ignore_errors=True)

    def _list(self):
        commandline = "git ls-remote --heads {0}".format(self.url_string)
        _, sout, serr = self.subprocess_popen(commandline)
        log.Debug(serr)
        refs = 'refs/heads/'
        def filter_branch(refs_line):
            try:
                return self.__decode_name(refs_line)
            except:
                return None
        return filter(None, [ filter_branch(line.split(refs)[1]) for line in sout.strip().split('\n') ])

    def _delete(self, filename):
        temp_dir = tempfile.mkdtemp(prefix='gitback-')
        repo_dir = os.path.join(temp_dir, self.repo_dir_name)
        commandline = "git clone --single-branch --branch master --depth 1 {0} {1}".format(self.url_string, repo_dir)
        self.subprocess_popen(commandline)
        prev_dir = os.getcwd()
        os.chdir(repo_dir)
        branchname = self.__encode_name(filename)
        commandline = "git push origin :{0}".format(branchname)
        self.subprocess_popen(commandline)
        os.chdir(prev_dir)
        shutil.rmtree(temp_dir, ignore_errors=True)

    def _test(self):
        import pprint
        print "test {0}".format(self.parsed_url.url_string)
        print "list:"
        orig_list = sorted(self._list())
        pprint.pprint(orig_list)
        temp_fd, temp_path = tempfile.mkstemp(prefix='testgitback-')
        temp_name = os.path.basename(temp_path)
        print "create {0}".format(temp_path) 
        os.write(temp_fd, "test file")
        os.close(temp_fd)
        print "put temp file"
        self._put(duplicity.path.Path(temp_path), temp_name) 
        try:
            after_list = self._list()
            print after_list
            self._list().index(temp_name)
            print "file seems to exists in remote"
        except:
            print "!!! DO NOT SEE FILE IN REMOTE AFTER UPLOAD !!!!!!"
            return
        restored = temp_path + 'restored'
        self._get(temp_name, duplicity.path.Path(restored))        
        with open(temp_path, "r") as file1:
            content1 = file1.read()
        with open(restored, "r") as file2:
            content2 = file2.read()
        if (content1==content2):
            print "restored file is equal to original - OK"
            os.remove(temp_path)
            os.remove(restored)
        else:
            print "!!! RESTORED FILE DOES NOT MATCH ORIGINAL ONE!!!!!!!!!!!!"
            return
        print "delete remote tempfile"
        self._delete(temp_name)
        if '|'.join(orig_list) == '|'.join(sorted(self._list())):
            print "file seems to be deleted on remote"
        else:
            print "!!! remote file list does not match current list after put() and delete() !!!"
        
        

duplicity.backend.register_backend("git+ssh", GitBackend)

'''
def main():
    import sys
    import random

    if len(sys.argv) == 3 and sys.argv[1] == 'test':
        URL = sys.argv[2]
        log.setup()
        log.setverbosity(log.DEBUG)
        parsed_url = duplicity.backend.ParsedUrl(URL)
        testback = GitBackend(parsed_url)
        testback._test()
        

if __name__ == "__main__":
    main()
'''
